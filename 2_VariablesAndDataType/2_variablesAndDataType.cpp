#include <iostream>
#include <unistd.h>
using namespace std;
/*
using namespace std; 
Questa dichiarazione indica al compilatore di utilizzare automaticamente il namespace std per tutte le funzioni, le classi e gli oggetti contenuti in esso.
Ad esempio ora possiamo rimuovere il namespace std:: per printare a schermo le nostre variabili.
*/

int main()
{
    char myCharacter; //abbiamo così dichiarato la variabile myCharacter come "char" che rappresenta un singolo carattere alfanumerico, come ad esempio una lettera dell'alfabeto, un numero o un carattere speciale come il punto o la virgola
    myCharacter = 'y';
    cout << myCharacter << "/n"; //dopo la var myCharacter, ho appeso con altre << la nuova linea
    cout << myCharacter << endl; //qui faccio praticamente la stessa cosa, usando il namespace std che mette a disposizione una serie di funzioni tra cui endl che sta per 

    int myInt;
    myInt = 13;
    cout << myInt;

    sleep(2);
    return 0;
}